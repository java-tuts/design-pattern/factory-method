package com.d4z;

import java.lang.reflect.Constructor;
import java.util.HashMap;
import java.util.Map;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.http.HttpHeaders;
import io.vertx.core.http.HttpServer;
import io.vertx.core.http.HttpServerOptions;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.BodyHandler;

interface IActionHandler {
	public JsonObject handle();
}

class LoginHandler implements IActionHandler {
	public LoginHandler() { }
	@Override
	public JsonObject handle() {
		return new JsonObject().put("errors", "Invalid userID");
	}
}

class LogoutHandler implements IActionHandler {
	public LogoutHandler() { }
	@Override
	public JsonObject handle() {
		return new JsonObject().put("errors", null);
	}
}

class ClassHandlerFactory {
	private Map<String, String> actionMapping;
	
	public ClassHandlerFactory() {
		actionMapping = new HashMap<String, String>();
		actionMapping.put("login", "com.d4z.LoginHandler");
		actionMapping.put("logout", "com.d4z.LogoutHandler");
	}
	
	public IActionHandler getHandler(String action) throws Exception {
		final String classHandler = actionMapping.get(action);
		if(classHandler != null) {
			Class<?> myClass = Class.forName(classHandler);
			Constructor<?> constructor = myClass.getConstructor();
			IActionHandler actionHandler = (IActionHandler) constructor.newInstance();
			return actionHandler;
		}
		else {
			throw new Exception("ActionNotFound");
		}
	}
}

public class MyHttpServer extends AbstractVerticle {

	@Override
	public void start() throws Exception {
		super.start();
		HttpServerOptions httpOption = new HttpServerOptions();
		Router router = Router.router(this.vertx);
		final ClassHandlerFactory factory = new ClassHandlerFactory();

		router.post("/api/:action").handler(BodyHandler.create()).handler(rtx -> {
			final String action = rtx.pathParam("action");
			JsonObject response;
			int statusCode = 200;
			
			try {
				IActionHandler actionHandler = factory.getHandler(action);
				response = actionHandler.handle();
			} catch (Exception e) {
				e.printStackTrace();
				response = new JsonObject().put("errors", e.getMessage());
				statusCode = 500;
			}
			
			rtx.response()
			 .putHeader(HttpHeaders.CONTENT_TYPE, "application/json")
			 .setStatusCode(statusCode)
			 .end(response.toString());
		});

		HttpServer server = vertx.createHttpServer(httpOption);
		server.requestHandler(router).listen(33000, res -> {
			if (res.succeeded()) {
				System.out.println("Started http://localhost:33000/api/");
			} else {
				res.cause().printStackTrace();
			}
		});
	}
}
